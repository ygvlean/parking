package org.parking.controller;

import org.parking.entity.ParkingEntity;
import org.parking.exception.WrongRequestException;
import org.parking.service.CarsService;
import org.parking.service.ParkingHistoryService;
import org.parking.service.ParkingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parking")
public class ParkingController {

    private final ParkingService parkingService;
    private final CarsService carsService;

    public ParkingController(ParkingService parkingService, CarsService carsService) {
        this.parkingService = parkingService;
        this.carsService = carsService;
    }

    @PostMapping
    public ResponseEntity createParkingLot(@RequestBody ParkingEntity lot) {
        try {
            parkingService.createParkingLot(lot);
            return ResponseEntity.ok("Parking lot " + lot.getId() + " initialised");
        } catch (WrongRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("неизвестная ошибка");
        }
    }

    //    public ParkingEntity createNewLot(){}

    @PostMapping("/carPark/{parkingEntityId}/{carId}")
    public ResponseEntity parkCar(@PathVariable("parkingEntityId") Integer parkingEntityId, @PathVariable("carId") int carId) {
        try {
            parkingService.parkCar(parkingEntityId, carId);
            return ResponseEntity.ok("Car " + carId + " parked on lot " + parkingEntityId);
        } catch (WrongRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("ошибка");
        }
    }

    @GetMapping("/carRemove/{unique_data}")
    public ResponseEntity removeCar(@PathVariable("unique_data") Integer lotId, @RequestParam ("unique_data") Integer unique_data) {
        try {
            parkingService.removeCar(lotId, unique_data);
            return ResponseEntity.ok("Car removed from lot " + lotId);
        } catch (WrongRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("ошибка");
        }
    }

    @GetMapping("/")
    public ResponseEntity getParking() {
        try {
            return ResponseEntity.ok("parking ok");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("ошибка");
        }
    }

    @GetMapping("/status")
    public ResponseEntity<List<ParkingEntity>> parkingStatus() {
        List<ParkingEntity> parkingEntityList = parkingService.parkingStatus();
        return new ResponseEntity<>(parkingEntityList, HttpStatus.OK);
    }

}
