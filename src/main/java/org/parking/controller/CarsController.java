package org.parking.controller;

import org.parking.entity.CarsEntity;
import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cars")
public class CarsController {
    @Autowired
    private CarsService carsService;

    @PostMapping
    public ResponseEntity createCar(@RequestParam Integer placerId, @RequestBody CarsEntity car) {
        try {
            carsService.createCar(placerId, car);
            return ResponseEntity.ok("Car created");
        } catch (WrongRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("ошибка");
        }
    }
}

