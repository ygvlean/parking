package org.parking.controller;

import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.service.HumansService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/humans")
public class HumansController {
    @Autowired
    private HumansService humansService;

    @PostMapping
    public ResponseEntity createHuman(@RequestBody HumansEntity human) {
        try {
            humansService.createHuman(human);
            return ResponseEntity.ok("Human " + human.getId() + " created");
        } catch (WrongRequestException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body("ошибка");
        }
    }
}
