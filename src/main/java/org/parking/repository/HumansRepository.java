package org.parking.repository;

import org.parking.entity.HumansEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumansRepository extends CrudRepository<HumansEntity, Integer> {
}
