package org.parking.repository;

import org.parking.entity.CarsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarsRepository extends CrudRepository<CarsEntity, Integer> {
}
