package org.parking.repository;

import org.parking.entity.ParkingHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingHistoryRepository extends JpaRepository<ParkingHistoryEntity, Integer> {
}
