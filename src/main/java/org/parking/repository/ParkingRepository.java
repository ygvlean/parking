package org.parking.repository;

import org.parking.entity.CarsEntity;
import org.parking.entity.ParkingEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface
ParkingRepository extends CrudRepository<ParkingEntity, Integer> {
    ParkingEntity getParkingEntitiesById(Integer integer);
}
