package org.parking.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
public class HumansEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int unique_data;
    public HumansEntity() {
    }

    public int getUnique_data() {
        return unique_data;
    }

    public void setUnique_data(int unique_data) {
        this.unique_data = unique_data;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
