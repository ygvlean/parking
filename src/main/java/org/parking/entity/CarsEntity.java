package org.parking.entity;

import javax.persistence.*;
import java.util.Optional;

@Entity
public class CarsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String gos_number;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "placer", referencedColumnName = "id")
    private HumansEntity human;

    public CarsEntity() {
    }

    public HumansEntity getHuman() {
        return human;
    }

    public void setHuman(HumansEntity human) {
        this.human = human;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGos_number() {
        return gos_number;
    }

    public void setGos_number(String gos_number) {
        this.gos_number = gos_number;
    }
}

