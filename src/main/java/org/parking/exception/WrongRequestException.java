package org.parking.exception;

public class WrongRequestException extends Exception{
    public WrongRequestException(String message) {
        super(message);
    }
}
