package org.parking.exception;

public class CantAddParkingLotWithCarException extends Exception {
    public CantAddParkingLotWithCarException(String message) {
        super(message);
    }
}
