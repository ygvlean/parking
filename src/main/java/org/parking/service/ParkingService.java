package org.parking.service;

import org.parking.entity.ParkingHistoryEntity;
import org.parking.exception.WrongRequestException;
import org.parking.entity.ParkingEntity;
import org.parking.repository.CarsRepository;
import org.parking.repository.HumansRepository;
import org.parking.repository.ParkingHistoryRepository;
import org.parking.repository.ParkingRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ParkingService {
    private final ParkingRepository parkingRepository;

    private final HumansRepository humansRepository;
    private final CarsRepository carsRepository;
    private final ParkingHistoryRepository parkingHistoryRepository;

    public ParkingService(ParkingRepository parkingRepository, HumansRepository humansRepository, CarsRepository carsRepository, ParkingHistoryRepository parkingHistoryRepository) {
        this.parkingRepository = parkingRepository;
        this.humansRepository = humansRepository;
        this.carsRepository = carsRepository;
        this.parkingHistoryRepository = parkingHistoryRepository;
    }


    public ParkingEntity createParkingLot(ParkingEntity lot) throws WrongRequestException {
        if (lot.getCar() == null & lot.getParkingDate() == null) {
            return parkingRepository.save(lot);
        } else if (lot.getParkingDate() != null) {
            throw new WrongRequestException("Set field date null," + " you cant create parking lot with car and date accordingly");
        } else if (lot.getCar() != null) {
            throw new WrongRequestException("Set field car null," + " you cant create parking lot with car and date accordingly");
        } else throw new WrongRequestException("something wrong, car and date should be null");
    }

    public List<ParkingEntity> parkingStatus() {
        return (List<ParkingEntity>) parkingRepository.findAll();
    }

    public ParkingEntity parkCar(int parkingEntityId, int carId) throws WrongRequestException {
        if (parkingRepository.existsById(parkingEntityId) & carsRepository.existsById(carId)) {
            ParkingEntity parkingEntity = parkingRepository.findById(parkingEntityId).get();
            parkingEntity.setCar(carsRepository.findById(carId).get());
            parkingEntity.setParkingDate(new Date());
            return parkingRepository.save(parkingEntity);
        } else if (!parkingRepository.existsById(parkingEntityId)) {
            throw new WrongRequestException("parking lot " + parkingEntityId + " don't exist");
        } else if (!carsRepository.existsById(carId)) {
            throw new WrongRequestException("car " + carId + " don't exist");
        } else throw new WrongRequestException("something went wrong...");
    }

    public ParkingEntity removeCar(Integer lotId, Integer unique_data) throws WrongRequestException {
        if (parkingRepository.existsById(lotId) && parkingRepository.findById(lotId).get().getCar() != null) {
            if (humansRepository.findById(parkingRepository.findById(lotId).get().getCar().getHuman().getId()).get().getUnique_data() == unique_data) {
                ParkingHistoryEntity parkingHistoryEntity = new ParkingHistoryEntity();
                parkingHistoryEntity.setCar_id(parkingRepository.findById(lotId).get().getCar().getId());
                parkingHistoryEntity.setParkingDate(parkingRepository.findById(lotId).get().getParkingDate());
                parkingHistoryEntity.setPlacer_id(parkingRepository.findById(lotId).get().getCar().getHuman().getId());
                parkingHistoryEntity.setRemoveDate(new Date());
                ParkingEntity parkingEntity = parkingRepository.findById(lotId).get();
                parkingEntity.setCar(null);
                parkingEntity.setParkingDate(null);
                parkingHistoryRepository.save(parkingHistoryEntity);
                return parkingRepository.save(parkingEntity);
            } else throw new WrongRequestException("wrong unique data");
        } else if (parkingRepository.findById(lotId).isEmpty()) {
            throw new WrongRequestException("parking lot doesn't exist");
        } else throw new WrongRequestException("parking lot already empty");
    }
}

