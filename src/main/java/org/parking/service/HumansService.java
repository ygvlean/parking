package org.parking.service;

import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.repository.HumansRepository;
import org.springframework.stereotype.Service;

@Service
public class HumansService {
    private final HumansRepository humansRepository;

    public HumansService(HumansRepository humansRepository) {
        this.humansRepository = humansRepository;
    }

    public HumansEntity createHuman(HumansEntity human) throws WrongRequestException {
        if (human.getName() != null & human.getUnique_data() > 0 & human.getUnique_data() <= 9999 ){
            return humansRepository.save(human);
        } else if (human.getName() == null){
            throw new WrongRequestException("Name cant be empty!");
        } else if (human.getUnique_data() < 0 | human.getUnique_data() > 9999) {
            throw new WrongRequestException("your unique data should be 4 number password");
        }
        throw new WrongRequestException("Name cant be empty, unique date " +
                "should be any of 1 to 9999 numbers");
    }
}
