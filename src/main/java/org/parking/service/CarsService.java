package org.parking.service;

import org.parking.entity.CarsEntity;
import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.repository.CarsRepository;
import org.parking.repository.HumansRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@Service
public class CarsService {
    private final CarsRepository carsRepository;
    private final HumansRepository humansRepository;

    public CarsService(CarsRepository carsRepository, HumansRepository humansRepository) {
        this.carsRepository = carsRepository;
        this.humansRepository = humansRepository;
    }

    public CarsEntity createCar(Integer placer, CarsEntity car) throws WrongRequestException {
        if (car.getGos_number() != null & humansRepository.existsById(placer)) {
            car.setHuman(humansRepository.findById(placer).get());
            return carsRepository.save(car);
        } else if (car.getGos_number() == null)
            throw new WrongRequestException("Something wrong with gos number");
        else if (!humansRepository.existsById(placer)) {
            throw new WrongRequestException("owner not found");
        } else if (car.getHuman() == null) {
            throw new WrongRequestException("car should have placer");
        } else throw new WrongRequestException("something went wrong...");
    }//todo спросить про requestparam null

    public CarsEntity getCarById(Integer carId) {
        CarsEntity carsEntity = carsRepository.findById(carId).get();
        return carsEntity;
    }
}
