package org.parking.service;

import org.parking.repository.ParkingHistoryRepository;
import org.springframework.stereotype.Service;

@Service
public class ParkingHistoryService {
    private final ParkingHistoryRepository parkingHistoryRepository;

    public ParkingHistoryService(ParkingHistoryRepository parkingHistoryRepository) {
        this.parkingHistoryRepository = parkingHistoryRepository;
    }

}
