package org.parking.CarsService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.parking.entity.CarsEntity;
import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.repository.CarsRepository;
import org.parking.repository.HumansRepository;
import org.parking.service.CarsService;
import org.parking.service.HumansService;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarsServiceTest {
    @Mock
    private CarsRepository carsRepository;
    @InjectMocks
    private CarsService carsService;
    @Mock
    private HumansRepository humansRepository;
    @InjectMocks
    private HumansService humansService;
    @Test
    public void carCreateSuccessfully() throws WrongRequestException {
        CarsEntity carsEntity = new CarsEntity();
        CarsEntity carsEntity2 = new CarsEntity();
        HumansEntity humansEntity = new HumansEntity();
        humansEntity.setId(1);
        humansEntity.setName("pasha");
        humansEntity.setUnique_data(124);
        carsEntity.setGos_number("ek777x77");
        carsEntity.setHuman(humansEntity);

        Mockito.when(humansRepository.existsById(1)).thenReturn(true);
        Mockito.when(humansRepository.findById(1)).thenReturn(Optional.of(humansEntity));
        Mockito.when(carsRepository.save(carsEntity)).thenReturn(carsEntity);

        carsService.createCar(1, carsEntity);
        WrongRequestException thrown = Assertions
                .assertThrows(WrongRequestException.class, () -> {
                    carsService.createCar(1, carsEntity2);
                }, "WrongRequestException error was expected");

        Assertions.assertEquals("Something wrong with gos number",
                thrown.getMessage());

        Assertions.assertNotNull(carsEntity);
        Assertions.assertEquals("ek777x77", carsEntity.getGos_number());
        Assertions.assertEquals(humansEntity, carsEntity.getHuman());

    }
}
