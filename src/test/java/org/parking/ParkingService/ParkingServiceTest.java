package org.parking.ParkingService;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.parking.entity.ParkingEntity;
import org.parking.exception.WrongRequestException;
import org.parking.repository.ParkingRepository;
import org.parking.service.ParkingService;

import java.util.Date;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)//todo узнать че значит
public class ParkingServiceTest {
    @Mock
    private ParkingRepository parkingRepository;
    @InjectMocks
    private ParkingService parkingService;

    @Test
    public void createParkingLotIsOk() throws WrongRequestException {
        ParkingEntity parkingEntity = new ParkingEntity();
        ParkingEntity parkingEntity2 = new ParkingEntity();
        parkingEntity2.setParkingDate(new Date());
        Mockito.when(parkingRepository.save(parkingEntity)).thenReturn(parkingEntity);
        Mockito.when(parkingRepository.save(parkingEntity2)).thenReturn(parkingEntity2);

        parkingService.createParkingLot(parkingEntity);

        WrongRequestException thrown = Assertions
                .assertThrows(WrongRequestException.class, () -> {
                    parkingService.createParkingLot(parkingEntity2);
                }, "NumberFormatException error was expected");

        Assertions.assertEquals("Set field date null, you cant create parking lot with car and date accordingly",
                thrown.getMessage());

        Assertions.assertNotNull(parkingEntity);
        Assertions.assertNull(parkingEntity.getParkingDate());
        Assertions.assertNull(parkingEntity.getCar());
    }
}
