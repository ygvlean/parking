package org.parking.HumansService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.parking.entity.CarsEntity;
import org.parking.entity.HumansEntity;
import org.parking.exception.WrongRequestException;
import org.parking.repository.HumansRepository;
import org.parking.service.HumansService;

import java.util.Date;

@ExtendWith(MockitoExtension.class)

public class HumansServiceTest {
    @Mock
    private HumansRepository humansRepository;
    @InjectMocks
    private HumansService humansService;
    @Test
    public void humanCreateSuccessfully () throws WrongRequestException {
        HumansEntity humansEntity = new HumansEntity();
        HumansEntity humansEntity2 = new HumansEntity();
        humansEntity.setName("pasha");
        humansEntity.setUnique_data(124);
        Mockito.when(humansRepository.save(humansEntity)).thenReturn(humansEntity);

        humansService.createHuman(humansEntity);

        WrongRequestException thrown = Assertions
                .assertThrows(WrongRequestException.class, () -> {
                    humansService.createHuman(humansEntity2);
                }, "WrongRequestException error was expected");

        Assertions.assertEquals("Name cant be empty!",
                thrown.getMessage());

        Assertions.assertNotNull(humansEntity);
        Assertions.assertEquals("pasha", humansEntity.getName());
        Assertions.assertEquals(124, humansEntity.getUnique_data());
    }
}
//todo узнать про покрытие кода и организацию